package ru.nlmk.jse29.service;

import com.google.common.math.LongMath;

public class Service {

  public int getFibonacciValue(int n) {

    if (n <= 1) {
      return 0;
    } else if (n == 2) {
      return 1;
    } else  {
      return getFibonacciValue(n - 1) + getFibonacciValue(n - 2);
    }
  }

  public long sum(String arg1, String arg2){

    if ((!arg1.matches("^-?\\d+$")) || (!arg1.matches("^-?\\d+$")))
      throw new IllegalArgumentException();

    return Long.parseLong(arg1) + Long.parseLong(arg2);
  }

  public long factorial(String arg){
    if ((!arg.matches("^-?\\d+$")) || (Long.parseLong(arg) < 0))
      throw new IllegalArgumentException();

    return LongMath.factorial(Integer.parseInt(arg));
  }

  public long[] fibonacci(String arg) {
    if ((!arg.matches("^-?\\d+$")) || (Long.parseLong(arg) < 1))
      throw new IllegalArgumentException();

    long num = Long.parseLong(arg);
    long sum = 0;
    long n = Long.MAX_VALUE;
    int nequal = -1;
    long result[];

    for (int i = 1; i < n; i++){
      sum += getFibonacciValue(i);

      if (num == sum){
        nequal = i;
        break;
      } else if (sum > num){
        break;
      }
    }

    if (nequal == -1)
      throw new IllegalArgumentException();
    else {
      result = new long[nequal];
      
      for (int i = 0; i < nequal; i++){
        result[i] = getFibonacciValue(i + 1);
      }
      return result;
    }  
  }

}

