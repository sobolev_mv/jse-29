package ru.nlmk.jse29.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class ServiceTest {

  private Service myService;

  @BeforeEach
  private void setup(){
    myService = new Service();
  }

  @Test
  void sumCorrect() {
    assertEquals(1000000, myService.sum("500000", "500000"));
  }

  @Test
  void sumExceptionArg1() {
    assertThrows(IllegalArgumentException.class, ()-> myService.sum("asde", "500000"));
  }

  @Test
  void sumExceptionArg2() {
    assertThrows(IllegalArgumentException.class, ()-> myService.sum("500000", null));
  }

  @Test
  void factorialCorrect() {
    assertEquals(6, myService.factorial("3"));
  }

  @Test
  void factorialException() {
    assertThrows(IllegalArgumentException.class, ()-> myService.factorial("-1"));
  }

  @Test
  void fibonacciCorrect() {
    long[] arrExp  = new long[]{0,1,1,2,3};
    long[] arrActual  = myService.fibonacci("7");
    assertArrayEquals(arrExp, arrActual);
  }

  @Test
  void fibonacciException() {
    assertThrows(IllegalArgumentException.class, ()-> myService.fibonacci("8"));
  }
}